/* 
 * ✅ Use the Coinlore API (Coins) 
 *    https://www.coinlore.com/cryptocurrency-data-api
 * 
 *    Get 10 coins per "page"
 */

// VARIABLES
const table = document.querySelector('table');
const currencyDisplayContainer = document.querySelector('table tbody');
const nextSlide = document.querySelector('table .next');
const prevSlide = document.querySelector('table .prev');

class Coins {
  getCoins() {
    const url = 'https://api.coinlore.com/api/tickers/';
    const result = fetch(url)
    .then(res => res.json())
    .then(data => {
      let coins = data.data;
      coins.map(item => {
        const { name, symbol, price_usd, csupply } = item;
        return { name, symbol, price_usd, csupply };
      });
      return coins;
    });
    return result;
  }
}

class UI {
  displayCurrency(currency) {
    currency = currency.map(currency => {
      const { name, symbol, price_usd, csupply } = currency;
      const currencyHTML = `
        <tr>
          <td data-th="💰 Coin">${name}</td>
          <td data-th="📄 Code">${symbol}</td>
          <td data-th="🤑 Price">$ ${price_usd}</td>
          <td data-th="📉 Total Supply">${csupply} BTC</td>
        </tr>
      `;
      return currencyHTML;
    });

    let currentPage = 1;
    const listPerPage = 10;

    const numPages = () => {
      return Math.ceil(currency.length / listPerPage);
    };

    const prevPage = () => {
      if (currentPage > 1) {
        currentPage--;
        changePage(currentPage);
      }
    };

    const nextPage = () => {
      if (currentPage < numPages()) {
        currentPage++;
        changePage(currentPage);
      }
    };

    nextSlide.addEventListener('click', () => {
      nextPage();
    });

    prevSlide.addEventListener('click', () => {
      prevPage();
    });

    const changePage = (page) => {

      if (page < 1) {
        page = 1;
      }

      if (page > numPages()) {
        page = numPages();
      }

      // Clear table and update new values 
      currencyDisplayContainer.innerHTML = '';

      for (
        let i = (page - 1) * listPerPage;
        i < (page * listPerPage);
        i++
      ) {
        currencyDisplayContainer.innerHTML += currency[i];
      }

      if (page > 1) {
        prevSlide.classList.remove('hidden');
      } else if (page <= 1) {
        prevSlide.classList.add('hidden');
      }

      if (page == numPages()) {
        nextSlide.classList.add('hidden');
      } else {
        nextSlide.classList.remove('hidden');
      }
    };

    changePage(1); // loads initial 10 list
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const coin = new Coins();
  const ui = new UI();

  coin.getCoins().then(coins => {
    ui.displayCurrency(coins);
  });
});